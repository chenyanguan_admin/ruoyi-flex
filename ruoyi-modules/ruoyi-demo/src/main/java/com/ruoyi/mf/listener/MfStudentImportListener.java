package com.ruoyi.mf.listener;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.core.utils.ValidatorUtils;
import com.ruoyi.common.excel.core.ExcelListener;
import com.ruoyi.common.excel.core.ExcelResult;
import com.ruoyi.mf.domain.bo.MfStudentBo;
import com.ruoyi.mf.domain.vo.*;
import com.ruoyi.mf.service.*;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 学生信息表自定义导入
 *
 * @author 数据小王子
 */
@Slf4j
public class MfStudentImportListener extends AnalysisEventListener<MfStudentImportVo> implements ExcelListener<MfStudentImportVo> {
    private final IMfStudentService mfStudentService;

    private final Boolean isUpdateSupport;
    private int successNum = 0;
    private int failureNum = 0;
    private final StringBuilder successMsg = new StringBuilder();
    private final StringBuilder failureMsg = new StringBuilder();

    public MfStudentImportListener(Boolean isUpdateSupport) {
        this.mfStudentService = SpringUtils.getBean(IMfStudentService.class);
        this.isUpdateSupport = isUpdateSupport;
    }

    @Override
    public void invoke(MfStudentImportVo mfStudentVo, AnalysisContext context) {
        try {

            MfStudentBo mfStudentBo = BeanUtil.toBean(mfStudentVo, MfStudentBo.class);

            //TODO:根据某个字段，查询数据库表中是否存在记录，不存在就新增，存在就更新
            MfStudentVo mfStudentVo1 = null;

            //mfStudentVo1 = mfStudentService.selectBySomefield(mfStudentVo.getSomefield());
            if (ObjectUtil.isNull(mfStudentVo1)) {
                //不存在就新增
                mfStudentBo.setVersion(0);
                ValidatorUtils.validate(mfStudentBo);
                boolean inserted = mfStudentService.insert(mfStudentBo);

                if (inserted) {
                    successNum++;
                    successMsg.append("<br/>").append(successNum).append("、学生信息表 记录导入成功");
                    return;
                } else {
                    failureNum++;
                    failureMsg.append("<br/>").append(failureNum).append("、学生信息表 记录导入失败");
                    return;
                }
            } else if (isUpdateSupport) {
                //存在就更新
                mfStudentBo.setStudentId(mfStudentVo1.getStudentId());//主键
                mfStudentBo.setVersion(mfStudentVo1.getVersion());
                boolean updated = mfStudentService.update(mfStudentBo);
                if (updated) {
                    successNum++;
                    successMsg.append("<br/>").append(successNum).append("、学生信息表 记录更新成功");
                    return;
                } else {
                    failureNum++;
                    failureMsg.append("<br/>").append(failureNum).append("、学生信息表 记录更新失败");
                    return;
                }
            }
        } catch (Exception e) {
            failureNum++;
            String msg = "<br/>" + failureNum + "、学生信息表 记录导入失败：";
            failureMsg.append(msg).append(e.getMessage());
            log.error(msg, e);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }

    @Override
    public ExcelResult<MfStudentImportVo> getExcelResult() {
        return new ExcelResult<>() {

            @Override
            public String getAnalysis() {
                if (failureNum > 0) {
                    failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据没有成功导入，错误如下：");
                    throw new ServiceException(failureMsg.toString());
                } else {
                    successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
                }
                return successMsg.toString();
            }

            @Override
            public List<MfStudentImportVo> getList() {
                return null;
            }

            @Override
            public List<String> getErrorList() {
                return null;
            }
        };
    }
}
