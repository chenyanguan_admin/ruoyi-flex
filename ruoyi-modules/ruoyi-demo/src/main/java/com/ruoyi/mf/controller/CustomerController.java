package com.ruoyi.mf.controller;

import java.util.List;
import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.core.core.domain.R;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.web.annotation.RepeatSubmit;
import com.ruoyi.common.web.core.BaseController;
import jakarta.annotation.Resource;
import com.ruoyi.mf.domain.vo.CustomerVo;
import com.ruoyi.mf.domain.bo.CustomerBo;
import com.ruoyi.mf.service.ICustomerService;

import com.ruoyi.common.orm.core.page.TableDataInfo;

/**
 * 客户主表Controller
 *
 * @author 数据小王子
 * 2024-01-06
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/mf/customer")
public class CustomerController extends BaseController
{
    @Resource
    private ICustomerService customerService;

    /**
     * 查询客户主表列表
     */
    @SaCheckPermission("mf:customer:list")
    @GetMapping("/list")
    public TableDataInfo<CustomerVo> list(CustomerBo customerBo)
    {
        return customerService.selectPage(customerBo);
    }

    /**
     * 导出客户主表列表
     */
    @SaCheckPermission("mf:customer:export")
    @Log(title = "客户主表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CustomerBo customerBo)
    {
        List<CustomerVo> list = customerService.selectList(customerBo);
        ExcelUtil.exportExcel(list, "客户主表", CustomerVo.class, response);
    }

    /**
     * 获取客户主表详细信息
     */
    @SaCheckPermission("mf:customer:query")
    @GetMapping(value = "/{customerId}")
    public R<CustomerVo> getInfo(@PathVariable Long customerId)
    {
        return R.ok(customerService.selectById(customerId));
    }

    /**
     * 新增客户主表
     */
    @SaCheckPermission("mf:customer:add")
    @Log(title = "客户主表", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping
    public R<Void> add(@Validated @RequestBody CustomerBo customerBo)
    {
        boolean inserted = customerService.insert(customerBo);
        if (!inserted) {
            return R.fail("新增客户主表记录失败！");
        }
        return R.ok();
    }

    /**
     * 修改客户主表
     */
    @SaCheckPermission("mf:customer:edit")
    @Log(title = "客户主表", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping
    public R<Void> edit(@Validated @RequestBody CustomerBo customerBo)
    {
        Boolean updated = customerService.update(customerBo);
        if (!updated) {
            return R.fail("修改客户主表记录失败!");
        }
        return R.ok();
    }

    /**
     * 删除客户主表
     */
    @SaCheckPermission("mf:customer:remove")
    @Log(title = "客户主表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{customerIds}")
    public R<Void> remove(@PathVariable Long[] customerIds)
    {
        boolean deleted = customerService.deleteByIds(customerIds);
        if (!deleted) {
            return R.fail("删除客户主表记录失败!");
        }
        return R.ok();
    }
}
