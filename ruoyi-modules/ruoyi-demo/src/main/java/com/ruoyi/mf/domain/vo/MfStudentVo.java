package com.ruoyi.mf.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.mf.domain.MfStudent;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.excel.annotation.ExcelDictFormat;
import com.ruoyi.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;
import com.ruoyi.common.orm.core.domain.BaseEntity;

/**
 * 学生信息表视图对象 mf_student
 *
 * @author 数据小王子
 * @date 2024-04-12
 */
@Data
@ExcelIgnoreUnannotated
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = MfStudent.class)
public class MfStudentVo extends BaseEntity implements Serializable
{

    @Serial
    private static final long serialVersionUID = 1L;

     /** 编号 */
    @ExcelProperty(value = "编号")
    private Long studentId;

     /** 学生名称 */
    @ExcelProperty(value = "学生名称")
    private String studentName;

     /** 年龄 */
    @ExcelProperty(value = "年龄")
    private Integer studentAge;

     /** 爱好（0代码 1音乐 2电影） */
    @ExcelProperty(value = "爱好", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_student_hobby")
    private String studentHobby;

     /** 性别（1男 2女 3未知） */
    @ExcelProperty(value = "性别", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_user_gender")
    private String studentGender;

     /** 状态（0正常 1停用） */
    @ExcelProperty(value = "状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_student_status")
    private String studentStatus;

     /** 生日 */
    @ExcelProperty(value = "生日")
    private Date studentBirthday;

     /** 逻辑删除标志（0代表存在 1代表删除） */
    @ExcelProperty(value = "逻辑删除标志（0代表存在 1代表删除）")
    private Integer delFlag;



}
