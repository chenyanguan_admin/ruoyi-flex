package com.ruoyi.demo.service;

import java.util.List;
import com.ruoyi.demo.domain.DemoStudent;

/**
 * 学生信息单表(mb)Service接口
 * 
 * @author 数据小王子
 * 2023-07-11
 */
public interface IDemoStudentService 
{
    /**
     * 查询学生信息单表(mb)
     * 
     * @param studentId 学生信息单表(mb)主键
     * @return 学生信息单表(mb)
     */
    DemoStudent selectDemoStudentByStudentId(Long studentId);

    /**
     * 查询学生信息单表(mb)列表
     * 
     * @param demoStudent 学生信息单表(mb)
     * @return 学生信息单表(mb)集合
     */
    List<DemoStudent> selectDemoStudentList(DemoStudent demoStudent);

    /**
     * 新增学生信息单表(mb)
     * 
     * @param demoStudent 学生信息单表(mb)
     * @return 结果
     */
    int insertDemoStudent(DemoStudent demoStudent);

    /**
     * 修改学生信息单表(mb)
     * 
     * @param demoStudent 学生信息单表(mb)
     * @return 结果
     */
    int updateDemoStudent(DemoStudent demoStudent);

    /**
     * 批量删除学生信息单表(mb)
     * 
     * @param studentIds 需要删除的学生信息单表(mb)主键集合
     * @return 结果
     */
    int deleteDemoStudentByStudentIds(Long[] studentIds);

    /**
     * 删除学生信息单表(mb)信息
     * 
     * @param studentId 学生信息单表(mb)主键
     * @return 结果
     */
    int deleteDemoStudentByStudentId(Long studentId);
}
